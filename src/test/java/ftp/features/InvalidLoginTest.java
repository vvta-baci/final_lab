package ftp.features;

import ftp.steps.LoginErrorSteps;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.webdriver.WebDriverFacade;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("src/test/resources/invalid_credentials.csv")
public class InvalidLoginTest {
    @Managed(uniqueSession = true)
    WebDriverFacade driver;

    @Steps
    LoginErrorSteps loginErrorSteps;

    String server;
    String username;
    String password;

    @Issue("invalid login")
    @Test
    public void login_with_invalid_credentials_should_not_login_user() {
        loginErrorSteps.user_opens_login_page();
        loginErrorSteps.user_enters_data_and_clicks_login(server, username, password);
        loginErrorSteps.user_sees_error();
    }
}