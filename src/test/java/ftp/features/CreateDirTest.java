package ftp.features;

import ftp.steps.CreateDirSteps;
import ftp.steps.DeleteDirSteps;
import ftp.steps.DirSteps;
import ftp.steps.LoginSteps;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.webdriver.WebDriverFacade;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("src/test/resources/valid_credentials.csv")
public class CreateDirTest {
    @Managed(uniqueSession = true)
    WebDriverFacade driver;

    @Steps
    LoginSteps loginSteps;

    @Steps
    DirSteps dirSteps;

    @Steps
    CreateDirSteps createDirSteps;

    @Steps
    DeleteDirSteps deleteDirSteps;

    String server;
    String username;
    String password;

    @Issue("create dir")
    @Test
    public void create_dir() {
        loginSteps.user_opens_login_page();
        loginSteps.user_enters_data_and_clicks_login(server, username, password);
        dirSteps.user_is_logged_in(username);
        createDirSteps.user_creates_dir("serenity-test");
        deleteDirSteps.user_deletes_dir("serenity-test");
    }
}