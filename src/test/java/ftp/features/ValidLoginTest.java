package ftp.features;

import ftp.steps.DirSteps;
import ftp.steps.LoginSteps;
import ftp.steps.LogoutSteps;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.webdriver.WebDriverFacade;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("src/test/resources/valid_credentials.csv")
public class ValidLoginTest {
    @Managed(uniqueSession = true)
    WebDriverFacade driver;

    @Steps
    LoginSteps loginSteps;

    @Steps
    DirSteps dirSteps;

    @Steps
    LogoutSteps logoutSteps;

    String server;
    String username;
    String password;

    @Issue("valid login")
    @Test
    public void login_with_valid_credentials_should_login_user() {
        loginSteps.user_opens_login_page();
        loginSteps.user_enters_data_and_clicks_login(server, username, password);
        dirSteps.user_is_logged_in(username);
        dirSteps.user_logs_out();
        logoutSteps.user_is_logged_out();
    }
}