package ftp.steps;

import ftp.pages.LoginErrorPage;
import ftp.pages.LoginPage;
import net.thucydides.core.annotations.Step;

public class LoginErrorSteps {
    LoginPage loginPage;
    LoginErrorPage loginErrorPage;

    @Step
    public void user_opens_login_page() {
        loginPage.open();
    }

    @Step
    public void user_enters_data_and_clicks_login(String server, String name, String pass) {
        loginPage.clicks_login(); // for the cookie page
        loginPage.selects_server(server);
        loginPage.enters_name(name);
        loginPage.enters_password(pass);
        loginPage.clicks_login();
    }

    public void user_sees_error() {
        assert(loginErrorPage.has_error_message());
    }
}
