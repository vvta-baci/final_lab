package ftp.steps;

import ftp.pages.DeleteConfirmationPage;
import ftp.pages.DirPage;
import net.thucydides.core.annotations.Step;

public class DeleteDirSteps {
    DirPage dirPage;
    DeleteConfirmationPage deleteConfirmationPage;

    @Step
    public void user_deletes_dir(String dirName) {
        assert(dirPage.deletes_dir(dirName));
        assert(deleteConfirmationPage.sees_confirmation_message());
        deleteConfirmationPage.submits_form();
        assert(deleteConfirmationPage.sees_postprocessing_message());
        deleteConfirmationPage.goes_back();
    }
}
