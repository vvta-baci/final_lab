package ftp.steps;

import ftp.pages.DirPage;
import net.thucydides.core.annotations.Step;

public class DirSteps {
    DirPage dirPage;

    @Step
    public void user_is_logged_in(String username) {
        assert(dirPage.sees_user_in_dir_tree(username));
    }

    @Step
    public void user_logs_out() {
        dirPage.clicks_logout();
    }
}
