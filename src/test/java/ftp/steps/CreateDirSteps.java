package ftp.steps;

import ftp.pages.CreateDirPage;
import ftp.pages.CreateDirSuccessPage;
import ftp.pages.DirPage;
import net.thucydides.core.annotations.Step;

public class CreateDirSteps {
    DirPage dirPage;
    CreateDirPage createDirPage;
    CreateDirSuccessPage createDirSuccessPage;

    @Step
    public void user_creates_dir(String dirName) {
        dirPage.clicks_new_dir();
        createDirPage.fills_in_dir_name(dirName);
        createDirPage.submits_form();
        assert(createDirSuccessPage.sees_success_message());
        createDirSuccessPage.goes_back();
    }
}
