package ftp.steps;

import ftp.pages.LogoutPage;
import net.thucydides.core.annotations.Step;

public class LogoutSteps {
    LogoutPage logoutPage;

    @Step
    public void user_is_logged_out() {
        assert(logoutPage.sees_logout_message());
    }
}
