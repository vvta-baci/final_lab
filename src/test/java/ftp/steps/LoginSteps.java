package ftp.steps;

import ftp.pages.LoginPage;
import net.thucydides.core.annotations.Step;

public class LoginSteps {
    LoginPage loginPage;

    @Step
    public void user_opens_login_page() {
        loginPage.open();
    }

    @Step
    public void user_enters_data_and_clicks_login(String server, String name, String pass) {
        loginPage.clicks_login(); // for the cookie page
        loginPage.selects_server(server);
        loginPage.enters_name(name);
        loginPage.enters_password(pass);
        loginPage.clicks_login();
    }
}
