package ftp.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The page that confirms the deletion of a directory.
 */
public class DeleteConfirmationPage extends PageObject {
    @FindBy(id = "content_inner")
    WebElementFacade message;

    @FindBy(xpath = "//*[@id=\"CopyMoveDeleteForm\"]/li")
    List<WebElementFacade> messageList;

    @FindBy(id = "CopyMoveDeleteForm")
    WebElementFacade form;

    /**
     * @return Whether a "Are you sure you want to delete these directories and files?" confirmation is present.
     */
    public boolean sees_confirmation_message() {
        return message.getTextContent().contains("delete these directories");
    }

    /**
     * Confirms the deletion of the directory by submitting the form.
     */
    public void submits_form() {
        form.submit();
    }

    /**
     * @return Whether the deletion confirmation is present.
     */
    public boolean sees_postprocessing_message() {
        return containsText("All the selected directories and files have been processed.");
    }

    /**
     * Returns to the original dir page.
     *
     * We are executing the Javascript attached to the link that goes back instead of clicking it
     * because clicking it doesn't consistently work.
     */
    public void goes_back() {
        JavascriptExecutor driver = (JavascriptExecutor) getDriver();
        driver.executeScript("document.forms['CopyMoveDeleteForm'].state.value='browse';document.forms['CopyMoveDeleteForm'].state2.value='main';document.forms['CopyMoveDeleteForm'].submit();");
    }
}
