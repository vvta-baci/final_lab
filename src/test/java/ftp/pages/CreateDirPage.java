package ftp.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

/**
 * The "Create new directories" page.
 */
public class CreateDirPage extends PageObject {
    @FindBy(name = "newNames[1]")
    private WebElementFacade newDirInput;

    @FindBy(id = "NewDirForm")
    WebElementFacade form;

    /**
     * Enters the given dirname in the new directories form.
     * @param dirName
     */
    public void fills_in_dir_name(String dirName) {
        newDirInput.type(dirName);
    }

    /**
     * Submits the new directory form.
     */
    public void submits_form() {
        form.submit();
    }
}
