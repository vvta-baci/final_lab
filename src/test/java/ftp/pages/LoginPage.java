package ftp.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.cs.ubbcluj.ro/apps/ftp/index.php")
public class LoginPage extends PageObject {
    @FindBy(name = "ftpserver")
    WebElementFacade serverDropdown;

    @FindBy(name = "username")
    WebElementFacade username;

    @FindBy(name = "password")
    WebElementFacade password;

    @FindBy(id = "LoginButton1")
    WebElementFacade loginButton;

    /**
     * Selects the server with the given name in the servers dropdown.
     * @param server The server name.
     */
    public void selects_server(String server) {
        serverDropdown.selectByValue(server);
    }

    /**
     * Enters the username in the corresponding input.
     * @param name The username.
     */
    public void enters_name(String name) {
        username.type(name);
    }

    /**
     * Enters the password in the corresponding input.
     * @param pass The password.
     */
    public void enters_password(String pass) {
        password.type(pass);
    }

    /**
     * Clicks the login button.
     */
    public void clicks_login() {
        loginButton.click();
    }
}
