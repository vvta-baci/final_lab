package ftp.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

/**
 * The page displayed after logout.
 */
public class LogoutPage extends PageObject {
    @FindBy(className = "post")
    WebElementFacade message;

    /**
     * @return Whether a logout confirmation is present.
     */
    public boolean sees_logout_message() {
        return message.getTextContent().contains("logged out from the FTP server");
    }
}
