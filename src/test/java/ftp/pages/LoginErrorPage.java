package ftp.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

/**
 * The page displayed when users log in with invalid credentials.
 */
public class LoginErrorPage extends PageObject {
    @FindBy(className = "error-box")
    WebElementFacade message;

    /**
     * @return Whether an error message is present on the page.
     */
    public boolean has_error_message() {
        return message.getTextContent().contains("Unable to login");
    }
}
