package ftp.pages;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.JavascriptExecutor;

/**
 * The page that confirms the creation of a new directory.
 */
public class CreateDirSuccessPage extends PageObject {
    /**
     * @return Whether the confirmation is present.
     */
    public boolean sees_success_message() {
        return containsText("successfully created");
    }

    /**
     * Returns to the original dir page.
     *
     * We are executing the Javascript attached to the link that goes back instead of clicking it
     * because clicking it doesn't consistently work.
     */
    public void goes_back() {
        JavascriptExecutor driver = (JavascriptExecutor) getDriver();
        driver.executeScript("document.forms['NewDirForm'].state.value='browse';document.forms['NewDirForm'].state2.value='main';document.forms['NewDirForm'].submit();");
    }
}
