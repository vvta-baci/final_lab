package ftp.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.ElementNotVisibleException;

import java.util.List;
import java.util.Optional;

/**
 * The main ftp page.
 */
public class DirPage extends PageObject {
    @FindBy(xpath = "//*[@id=\"BrowseForm\"]/input[23]")
    private WebElementFacade dirPathForm;

    @FindBy(xpath = "//*[@id=\"StatusbarForm\"]/a[4]")
    private WebElementFacade logoutButton;

    @FindBy(xpath = "//*[@id=\"smallbutton\"]")
    private List<WebElementFacade> dirButtons;

    @FindBy( xpath = "//input[@type='checkbox']")
    private List<WebElementFacade> checkboxes;

    /**
     * Checks that the given user is present in the path displayed by the form.
     * @param username The user.
     * @return True if found.
     */
    public boolean sees_user_in_dir_tree(String username) {
        return dirPathForm.getValue().endsWith(username);
    }

    /**
     * Logs out.
     */
    public void clicks_logout() {
        logoutButton.click();
    }

    /**
     * Clicks the "New dir" button.
     */
    public void clicks_new_dir() {
        WebElementFacade button = getElementByValue(dirButtons, "New dir");

        if (button == null) {
            throw new ElementNotVisibleException("Cannot click new dir button!");
        }

        button.click();
    }

    /**
     * @param dirName The dir to delete.
     * @return Whether the deletion was successful.
     */
    public boolean deletes_dir(String dirName) {
        WebElementFacade checkbox = getElementByValue(checkboxes, dirName);

        if (checkbox == null) {
            throw new ElementNotVisibleException("Cannot click dir checkbox!");
        }

        checkbox.click();

        WebElementFacade button = getElementByValue(dirButtons, "Delete");

        if (button == null) {
            throw new ElementNotVisibleException("Cannot click delete dir button!");
        }

        button.waitUntilPresent();
        button.click();

        return true;
    }

    /**
     * Loops over a list of elements and returns the one with the desired value.
     * @param elements The elements.
     * @param value The value.
     * @return The element with the value.
     */
    private WebElementFacade getElementByValue(List<WebElementFacade> elements, String value) {
        Optional<WebElementFacade> element = elements.stream()
                .filter(e -> e.getValue().equalsIgnoreCase(value))
                .findFirst();

        return element.orElse(null);
    }
}
